#include "push_swap.h"
#include <time.h>
#include <stdlib.h>

int ft_check(t_list **head_a, t_list **head_b)
{
	t_list *tmp;

	if (*head_b != NULL)
		return (0);
	if (*head_a == NULL)
		return (0);
	if ((*head_a)->next == NULL)
		return (1);
	tmp = *head_a;
	while (tmp->next)
	{
		if (*((int *)tmp->content) > *((int *)tmp->next->content))
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

void    ft_show_stack(t_list *head_a, t_list *head_b)
{
	t_list *tmp1;
	t_list *tmp2;

	tmp1 = head_a;
	tmp2 = head_b;
	ft_printf("\x1B[34m%11s\x1B[0m", "STACK A");
	ft_printf("\x1B[35m%11s\x1B[0m\n", "STACK B");
	while (tmp1 || tmp2)
	{
		if (tmp1)
		{
			ft_printf("\x1B[34m%11d\x1B[0m", *((int *) tmp1->content));
			tmp1 = tmp1->next;
		}
		else
			ft_printf("%11c", ' ');
		if (tmp2)
		{
			ft_printf("\x1B[35m%11d\x1B[0m\n", *((int *) tmp2->content));
			tmp2 = tmp2->next;
		}
		else
			ft_printf("%11 \n");

	}
	ft_printf("\x1B[31m______________________\x1B[0m\n");



}

int main(int argc, char **argv)
{
	long int i;
	long int d;
	char *line;
	t_list *p = NULL;
	t_list *head_b = NULL;
	t_list *head_a = NULL;

	i = 1;
	while (i < argc)
	{
		d = ft_atoi(argv[i]);
		p = ft_lstnew(&d, sizeof(int));
		if (d > 2147483647 || d < -2147483648)
			return (ft_printf("Error\n"));
		ft_lstadd(&head_a, p);
		i++;
	}
	while (get_next_line(0, &line))
	{
		if (ft_strcmp(line, "sa") == 0)
			sa(&head_a);
		if (ft_strcmp(line, "sb") == 0)
			sa(&head_b);
		if (ft_strcmp(line, "ss") == 0)
		{
			sa(&head_a);
			sa(&head_b);
		}
		if (ft_strcmp(line, "pa") == 0)
			push(&head_b, &head_a);
		if (ft_strcmp(line, "pb") == 0)
			push(&head_a, &head_b);
		if (ft_strcmp(line, "ra") == 0)
			rotate(&head_a);
		if (ft_strcmp(line, "rb") == 0)
			rotate(&head_b);
		if (ft_strcmp(line, "rr") == 0)
		{
			rotate(&head_a);
			rotate(&head_b);
		}
		if (ft_strcmp(line, "rra") == 0)
			r_rotate(&head_a);
		if (ft_strcmp(line, "rrb") == 0)
			r_rotate(&head_b);
		if (ft_strcmp(line, "rrr") == 0)
		{
			r_rotate(&head_a);
			r_rotate(&head_b);
		}
		ft_show_stack(head_a, head_b);
		free(line);
	}
	if (ft_check(&head_a, &head_b))
	{
		ft_printf("\x1B[42m\x1B[30m%s", "          OK          \n");
	}
	else
		ft_printf("\x1b[41m\x1b[30m%s", "          KO          \n");
/*
	ft_show_stack(head_a, 'A');
	push(&head_a, &head_b);
//	sa(&head_a);
	ft_show_stack(head_a, 'A');
	ft_show_stack(head_b, 'B');
 */
	return (0);
}

void sa(t_list **head_a)
{
	t_list *tmp1;

	if (*head_a && (*head_a)->next)
	{
		tmp1 = (*head_a)->next;
		(*head_a)->next = (*head_a)->next->next;
		tmp1->next = *head_a;
		*head_a = tmp1;
	}
}

void push(t_list **head_a, t_list **head_b)
{
	t_list *tmp;

	if (*head_a == NULL)
		return ;
	tmp = *head_a;
	*head_a = (*head_a)->next;
	ft_lstadd(head_b, tmp);
}

void rotate(t_list **head)
{
	t_list *tmp1;
	t_list *tmp2;

	if (*head == NULL)
		return ;
	if ((*head)->next == NULL)
		return ;
	tmp1 = *head;
	*head = (*head)->next;
	tmp2 = *head;
	while (tmp2->next)
		tmp2 = tmp2->next;
	tmp2->next = tmp1;
	tmp1->next = NULL;
}

void r_rotate(t_list **head)
{
	t_list *tmp1;
	t_list *tmp2;

	if (*head == NULL)
		return ;
	if ((*head)->next == NULL)
		return ;
	tmp1 = *head;
	if (tmp1->next->next == NULL)
	{
		sa(head);
		return ;
	}
	while (tmp1->next->next)
		tmp1 = tmp1->next;
	tmp2 = tmp1->next;
	tmp1->next = NULL;
	tmp2->next = *head;
	*head = tmp2;
}
