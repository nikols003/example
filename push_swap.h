#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

#include "libft/libft.h"
#include "libft/ft_printf/libftprintf.h"

void rotate(t_list **head);
void    sa(t_list **head_a);
void push(t_list **head_a, t_list **head_b);
void r_rotate(t_list **head);

#endif